# Описание сервиса

Подробное описание сервиса находится в следующих файлах:

## Первичное описание

![docs/001-description](docs/001-description.md)

## Описание платформы

![docs/002-platforms.md](docs/002-platforms.md)

## Предварительная подготовка

![docs/003-before-provision.md](docs/003-before-provision.md)

## Развертка инфраструктуры

![docs/004-provision.md](docs/004-provision.md)

## Удаление инфраструктуры

![docs/005-destroy.md](docs/005-destroy.md)
