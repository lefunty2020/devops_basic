terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

# Будем создавать сразу через группу инстансов, чтобы потом если понадобится была возможность масштабирования
resource "yandex_compute_instance_group" "default" {
  description        = var.ig_description
  folder_id          = var.folder_id
  service_account_id = var.service_account_id
  instance_template {
    platform_id = var.platform_id
    resources {
      memory = var.ig_memory
      cores  = var.ig_cores
    }
    
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = var.ig_image_id
        size = var.ig_boot_disk_size
      }
    }
	
    network_interface {
      network_id = "${var.ig_network_id}"
      subnet_ids = ["${var.ig_subnet_id}"]
      nat = var.ig_network_nat
    }

    metadata = {
      ssh-keys = "ubuntu:${file(var.ig_public_key_file)}"
    }	
  }
 
  # Задаем количество машин 
  scale_policy {
    fixed_scale {
      size = var.ig_instance_count
    }
  }

  allocation_policy {
    zones = [var.zone]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = var.ig_target_group_name
    target_group_description = var.ig_target_group_description
  }
}

# Создание балансировщика
resource "yandex_lb_network_load_balancer" "default" {
  # Слушатель для балансировщика
  listener {
    name = "my-listener"
    port = 80
    target_port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  # Приатачиваем наш балансировщик к группе созданных хостов
  # т. к. на хостах будет висеть веб-сервис, то статус хостов будем определать по http
  attached_target_group {    
    target_group_id = "${yandex_compute_instance_group.default.load_balancer[0].target_group_id}"

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = var.balancer_health_path
      }
    }
  }
}

# Создаем ресурсные записи для балансировщика
resource "yandex_dns_recordset" "rs1" {
  for_each = toset(var.clouddns_domains)
  
  zone_id = var.clouddns_zone_id
  name    = "${each.key}."
  type    = "A"
  ttl     = 200
  data    = [ for list in yandex_lb_network_load_balancer.default.listener: list.external_address_spec.*.address ][0]  
}