output "instance_group" {
  value = yandex_compute_instance_group.default
}

output "dns_recordset" {
  value = yandex_dns_recordset.rs1
}

output "load_balancer" {
  value = yandex_lb_network_load_balancer.default
}