terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

resource "yandex_compute_instance" "default" {
  name        = var.name
  platform_id = var.platform_id
  zone        = var.zone
  description = var.description

  resources {
    cores  = var.cores
    memory = var.memory
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.boot_disk_size
    }
  }

  network_interface {   
    subnet_id       = var.subnet_id
    nat             = var.network_nat
    ip_address      = var.internal_ip
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.public_key_file)}"
  }
}