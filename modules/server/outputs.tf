output "server_name" {
  value = yandex_compute_instance.default.name
}

output "server_external_address" {
  value = yandex_compute_instance.default.network_interface[0].nat_ip_address
}

