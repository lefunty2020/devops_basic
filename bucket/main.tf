terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.yandex.token
  cloud_id  = var.yandex.cloud_id
  folder_id = var.yandex.folder_id
  zone      = var.yandex.zone
}

# Создаем bucket
resource "yandex_storage_bucket" "bucket-1" {
  access_key = var.yandex.bucket_access_key
  secret_key = var.yandex.bucket_secret_key
  bucket = "skillbox-infra-state"
  
  max_size = 1073741824
  folder_id = var.yandex.folder_id
  default_storage_class = "STANDARD"
  
  anonymous_access_flags {
    read = true
    list = true
    config_read = true
  }    
}