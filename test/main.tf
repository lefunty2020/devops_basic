terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "skillbox-infra-state"
    region     = "ru-central1"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = var.yandex.token
  cloud_id  = var.yandex.cloud_id
  folder_id = var.yandex.folder_id
  zone      = var.yandex.zone
}

data "yandex_vpc_network" "network-1" {
  name = "network1"
}

data "yandex_vpc_subnet" "subnet-2" {
  name = "subnet2"
}

data "yandex_dns_zone" "zone1" {
  name = "zone1"
}

# Создаем серверы, балансировщик и ресурсные записи в ДНС для тестового окружения окружения
module "infrastructure" {
  count              = 1
  source             = "../modules/infrastructure"

  folder_id             = var.yandex.folder_id
  service_account_id    = var.yandex.service_account_id  
  
  ig_description        = "Instance group for test infrastructure"
  ig_boot_disk_size     = var.instance_params.boot_disk_size
  ig_image_id           = var.instance_params.image_id
  ig_memory             = var.instance_params.memory
  ig_cores              = var.instance_params.cores
  ig_network_id         = data.yandex_vpc_network.network-1.id
  ig_subnet_id          = data.yandex_vpc_subnet.subnet-2.id
  ig_public_key_file    = "../.ssh/id_rsa.pub"
  ig_instance_count     = var.instance_params.count 
  
  balancer_health_path  = "/health"     
  
  clouddns_domains      = var.clouddns.balancer_domains_test
  clouddns_zone_id      = data.yandex_dns_zone.zone1.id
}

# Выполнение первоначальной конфигурации узлов
# Создаем инвентори файл
resource "null_resource" "create_inventory_file" {  
  provisioner "local-exec" {
    command = "echo '[servers]'>hosts"
  }

  depends_on = [module.infrastructure]
}

# Пишем в него информацию о созданных инстансах
resource "null_resource" "add_to_inventory_file" {
  count = var.instance_params.count

  provisioner "local-exec" {
    command = "echo '${module.infrastructure[0].instance_group.instances[count.index].name} ansible_host=${module.infrastructure[0].instance_group.instances[count.index].network_interface[0].nat_ip_address} ansible_connection=ssh ansible_user=ubuntu ansible_ssh_private_key_file=../.ssh/id_rsa' >>hosts"
  }

  depends_on = [null_resource.create_inventory_file]
}


# Делаем паузу для запуска инстансов
resource "null_resource" "ansible_waiting" {
  # Выполняем playbook, но перед этим делаем паузу на 240 секунд, чтобы хост успел гарантированно загрузиться
  provisioner "local-exec" {
    command = "/bin/sleep 120"
  }

  depends_on = [null_resource.add_to_inventory_file]
}

# Выполнение ansible-playbook для конфигурации инстансов после их создания
resource "null_resource" "ansible_run" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i hosts -v infra_playbook.yaml"
  }

  depends_on = [null_resource.ansible_waiting]
}
