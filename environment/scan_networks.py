import os
import platform
import threading
import socket
from datetime import datetime

ip_ranges = [[], []]

def scan_Ip(ip, index):
    comm = ping_com + ip
    response = os.popen(comm)
    data = response.readlines()
    for line in data:
        if 'ttl' in line:
            ip_ranges[index].append(ip)
            break


networks = [['test_infra.yaml', '192.168.11.'], ['prod_infra.yaml', '192.168.12.']]
start_point = 3
end_point = 253
ping_com = "ping -c 1 "

index = 0
for net_item in networks:
  network = net_item[1]

  os.system("echo ")
  for ip in range(start_point, end_point):
    addr = network + str(ip)
    potoc = threading.Thread(target=scan_Ip, args=[addr, index])
    potoc.start()

  index += 1

potoc.join()

print("Scanning completed.")

# Выводим результат в файл
for i in range(2):
  file = open("/root/" + networks[i][0], "w")
  file.write("  - targets: [")

  delimeter = ""
  for addr in ip_ranges[i]:
    file.write(delimeter + "\"" + addr +":9100\"")
    delimeter = ", "

  file.write("]")
  file.close()
