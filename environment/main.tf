terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "skillbox-infra-state"
    region     = "ru-central1"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = var.yandex.token
  cloud_id  = var.yandex.cloud_id
  folder_id = var.yandex.folder_id
  zone      = var.yandex.zone
}

# Создаем сеть 
resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

# Назначаем параметры подсети для созданной сети
# Подсеть для инфраструктурных машин
resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = var.yandex.zone
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["192.168.10.0/24"]
}

# Назначаем параметры подсети для созданной сети
# Подсеть для виртуальных машин с тестовой средой
resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet2"
  zone           = var.yandex.zone
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["192.168.11.0/24"]
}

# Назначаем параметры подсети для созданной сети
# Подсеть для виртуальных машин с продуктовой средой
resource "yandex_vpc_subnet" "subnet-3" {
  name           = "subnet3"
  zone           = var.yandex.zone
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["192.168.12.0/24"]
}

variable "server_names" {
  type     = list
  default  = ["logs", "monitoring", "runner"]
}

variable "server_disk_sizes" {
  type     = list
  default  = [20, 10, 12]
}

variable "server_memory" {
  type     = list
  default  = [4, 2, 4]
}

variable "server_descriptions" {
  type     = list
  default  = ["Logs (EFK)", "Monitoring server (grafana + prometheus)", "Runner for GitLab"] 
}

variable "server_ip" {
  type     = list
  default  = ["192.168.10.100", "192.168.10.101", "192.168.10.102"]
}


# Создаем серверы для окружения
module "server" {
  count           = length(var.server_names)
  source          = "../modules/server"
  
  name            = var.server_names[count.index]
  boot_disk_size  = var.server_disk_sizes[count.index]
  description     = var.server_descriptions[count.index]
  memory          = var.server_memory[count.index] 
  subnet_id       = yandex_vpc_subnet.subnet-1.id
  public_key_file = "../.ssh/id_rsa.pub" 
  internal_ip     = var.server_ip[count.index]
}

# Создаем DNS-зону
resource "yandex_dns_zone" "zone1" {
  name             = "zone1"
  description      = "This is the devops diploma zone"

  zone             = "${var.clouddns.domain}."
  public           = true
  private_networks = [yandex_vpc_network.network-1.id]
}

#Создаем ресурсную запись для каждого созданного инстанса
resource "yandex_dns_recordset" "rs" {
    for_each = { for server_info in module.server: server_info.server_name => server_info }
    
    zone_id  = yandex_dns_zone.zone1.id
    name     = "${each.value.server_name}.${var.clouddns.domain}."
    type     = "A"
    ttl      = 200
    data     = [ each.value.server_external_address ]
}

# Первоначальное создание инвентори файла
resource "null_resource" "create_ansible_config" {
  
  provisioner "local-exec" {
    command = "echo ' '>hosts"
  }
  
  depends_on = [yandex_dns_recordset.rs]
}
 
# т. к. корневой домен может быть разным, то сформируем инвентори файл для Ansible
resource "null_resource" "add_to_ansible_config" {
  for_each = { for server_info in module.server: server_info.server_name => server_info }
  
  # добавляем информацию о ВМ с раннерами
  provisioner "local-exec" {
    command = "echo '[${each.value.server_name}_servers]'>>hosts; echo '${each.value.server_name} ansible_host=${each.value.server_external_address} ansible_connection=ssh ansible_user=ubuntu ansible_ssh_private_key_file=../.ssh/id_rsa' >>hosts"
  }
  
  depends_on = [null_resource.create_ansible_config]
}

# Делаем паузу для запуска инстансов
resource "null_resource" "ansible_waiting" {
  # Выполняем playbook, но перед этим делаем паузу, чтобы хосты успели гарантированно загрузиться
  provisioner "local-exec" {
    command = "/bin/sleep 60"
  }

  depends_on = [null_resource.add_to_ansible_config]
}

# Конфигурируем инстанс с системой сбора логов
resource "null_resource" "ansible_logs_config" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i hosts -vvv logs_playbook.yaml"
  }

  depends_on = [null_resource.ansible_waiting]
}

# Конфигурируем инстанс с прометеусом и графаной
resource "null_resource" "ansible_monitoring_config" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i hosts -vvv monitoring_playbook.yaml"
  }

  depends_on = [null_resource.ansible_logs_config]
}

# Конфигурируем инстанс с раннером
resource "null_resource" "ansible_runner_config" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i hosts -vvv runner_playbook.yaml"
  }

  depends_on = [null_resource.ansible_monitoring_config]
}

# Выводим информацию о созданных виртуальных машинах
output "Environment-servers" {
  value = module.server
}

