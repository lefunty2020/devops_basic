#!/bin/bash

echo "Вы запустили удаления инфраструктуры проекта."
echo -n "Продолжим (Y/n)? "

read AGREEMENT

if [[ $AGREEMENT != "n" ]] && [[ $AGREEMENT != "N" ]] 
then

  echo -n "Удалить prod? (y/N): "
  read DELETE_ITEM_PROD
  echo -n "Удалить test? (y/N): "
  read DELETE_ITEM_TEST
  echo -n "Удалить environment? (y/N): "
  read DELETE_ITEM_ENV
  echo -n "Удалить bucket? (y/N): "
  read DELETE_ITEM_BUCKET
  
  if [[ $DELETE_ITEM_PROD = "y" ]] || [[ $DELETE_ITEM_PROD = "Y" ]]
  then
    cd prod
    terraform destroy -auto-approve
    cd ..
  fi

  if [[ $DELETE_ITEM_TEST == "y" ]] || [[ $DELETE_ITEM_TEST == "Y" ]]
  then
    cd test
    terraform destroy -auto-approve
    cd ..
  fi

  if [[ $DELETE_ITEM_ENV == "y" ]] || [[ $DELETE_ITEM_ENV == "Y" ]]
  then
    cd environment
    terraform destroy -auto-approve
    cd ..
  fi

  if [[ $DELETE_ITEM_BUCKET == "y" ]] || [[ $DELETE_ITEM_BUCKET == "Y" ]]
  then
    cd bucket
    terraform destroy -auto-approve
    cd ..
  fi
       
  echo "Удаление инфраструктуры завершено. "
else
  echo "Удаление инфраструктуры отменено."
fi

