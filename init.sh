#!/bin/bash

echo "Вы запустили скрипт инициализации terraform проекта."
echo "От вас потребуется ввести несколько параметров для инициализации bucket.\n"
echo "Конкретно: "
echo "  - access_key "
echo "  - secret_key "
echo -n "Продолжим (Y/n)? "

read AGREEMENT

if [[ $AGREEMENT != "n" ]] && [[ $AGREEMENT != "N" ]] 
then
  echo -n "Введите access_key: "
  read ACCESS_KEY
  
  echo -n "Введите secret_key: "
  read SECRET_KEY
  
  echo "*******************************************"
  echo "*   Инициализация bucket                  *"
  echo "*******************************************"
  cd bucket
  terraform init

  echo "*******************************************"
  echo "*   Инициализация environment             *"
  echo "*******************************************"  
  cd ../environment
  terraform init \
    -backend-config="access_key=$ACCESS_KEY" \
    -backend-config="secret_key=$SECRET_KEY" \
    -backend-config="key=environment.tfstate"
    
  echo "*******************************************"
  echo "*   Инициализация test                    *"
  echo "*******************************************"  
  cd ../test
  terraform init \
    -backend-config="access_key=$ACCESS_KEY" \
    -backend-config="secret_key=$SECRET_KEY" \
    -backend-config="key=test_infra.tfstate"

  echo "*******************************************"
  echo "*   Инициализация prod                    *"
  echo "*******************************************"
  cd ../prod
  terraform init \
    -backend-config="access_key=$ACCESS_KEY" \
    -backend-config="secret_key=$SECRET_KEY" \
    -backend-config="key=prod_infra.tfstate"
   
  echo "Инициализация завершена. "
else
  echo "Инициализация отменена."
fi

