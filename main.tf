terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "skillbox"
    region     = "ru-central1"

    skip_region_validation      = true
    skip_credentials_validation = true
  }
}

provider "yandex" {
  token     = var.yandex.token
  cloud_id  = var.yandex.cloud_id
  folder_id = var.yandex.folder_id
  zone      = var.yandex.zone
}


# Будем создавать сразу через группу инстансов, чтобы потом если понадобится была возможность масштабирования
resource "yandex_compute_instance_group" "ig-1" {
  name               = "fixed-ig-with-balancer-1"
  description        = "Instances group 1"
  folder_id          = var.yandex.folder_id
  service_account_id = var.yandex.service_account_id
  instance_template {
    platform_id = var.instance_params.platform_id
    resources {
      memory = var.instance_params.memory
      cores  = var.instance_params.cores
    }

    # Ubuntu 22.04.1 LTS
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = var.instance_params.image_id
        size = var.instance_params.boot_disk_size
      }
    }

    network_interface {
      network_id = "${yandex_vpc_network.network-1.id}"
      subnet_ids = ["${yandex_vpc_subnet.subnet-1.id}"]
      nat = true
    }

    metadata = {
      ssh-keys = "ubuntu:${file(var.environment.public_key_file)}"
    }	
  }
 
  # Количество машин увеличиваем на 3, 
  # т. к. +1 - раннер
  # +2 - мониторинг
  # +3 - графана
  scale_policy {
    fixed_scale {
      size = var.instance_params.count + 3
    }
  }

  allocation_policy {
    zones = [var.yandex.zone]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "target-group"
    target_group_description = "load balancer target group"
  }
}

# Создаем сеть 
resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

# Назначаем параметры подсети для созданной сети
resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = var.yandex.zone
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["192.168.10.0/24"]
}

# Создание балансировщика
resource "yandex_lb_network_load_balancer" "load-balancer-1" {
  name = "my-network-load-balancer"

  # Слушатель для балансировщика
  listener {
    name = "my-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  # Приатачиваем наш балансировщик к группе созданных хостов
  # т. к. на хостах будет висеть веб-сервис, то статус хостов будем определать по http
  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.ig-1.load_balancer[0].target_group_id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/health"
      }
    }
  }
}

# Создаем DNS-зону
resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "This is the devops diploma zone"

  zone             = "${var.clouddns.domain}."
  public           = true
  private_networks = [yandex_vpc_network.network-1.id]
}

# Создаем ресурсную запись для балансировщика
resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "${var.clouddns.balancer}.${var.clouddns.domain}."
  type    = "A"
  ttl     = 200
  data    = [ for list in yandex_lb_network_load_balancer.load-balancer-1.listener: list.external_address_spec.*.address ][0]
}

# Создаем ресурсную запись на главном домене если нужно
# если нужно приатачить балансер также на главный домен
resource "yandex_dns_recordset" "rs2" {
    count   = var.clouddns.link_balancer_to_main_domain ? 1 : 0
    zone_id = yandex_dns_zone.zone1.id
    name    = "${var.clouddns.domain}."
    type    = "A"
    ttl     = 200
    data    = [ for list in yandex_lb_network_load_balancer.load-balancer-1.listener: list.external_address_spec.*.address ][0]  
}

# Создаем ресурсные записи под каждую машину
resource "yandex_dns_recordset" "instances" {
    count   = var.instance_params.count
    zone_id = yandex_dns_zone.zone1.id
    name    = "${var.clouddns.instances_prefix}${count.index}.${var.clouddns.domain}."
    type    = "A"
    ttl     = 200
    data    = [ yandex_compute_instance_group.ig-1.instances[count.index].network_interface[0].nat_ip_address ]
}

# Создаем ресурсную запись для машины с раннерами
resource "yandex_dns_recordset" "ranners" {
    zone_id = yandex_dns_zone.zone1.id
    name    = "${var.clouddns.runners_prefix}0.${var.clouddns.domain}."
    type    = "A"
    ttl     = 200
    data    = [ yandex_compute_instance_group.ig-1.instances[var.instance_params.count].network_interface[0].nat_ip_address ]
}

# Создаем ресурсную запись для машины с системой мониторинга
resource "yandex_dns_recordset" "monitoring" {
    zone_id = yandex_dns_zone.zone1.id
    name    = "${var.clouddns.monitoring_prefix}0.${var.clouddns.domain}."
    type    = "A"
    ttl     = 200
    data    = [ yandex_compute_instance_group.ig-1.instances[var.instance_params.count + 1].network_interface[0].nat_ip_address ]
}

# Создаем ресурсную запись для машины с графаной
resource "yandex_dns_recordset" "grafana" {
    zone_id = yandex_dns_zone.zone1.id
    name    = "${var.clouddns.grafana_prefix}0.${var.clouddns.domain}."
    type    = "A"
    ttl     = 200
    data    = [ yandex_compute_instance_group.ig-1.instances[var.instance_params.count + 2].network_interface[0].nat_ip_address ]
}

# Выполнение первоначальной конфигурации узлов
# Создаем инвентори файл
resource "null_resource" "ansible_config1" {
 
  provisioner "local-exec" {
    command = "mkdir tmp 2>/dev/null; echo '[servers]'>tmp/hosts"
  }

  depends_on = [yandex_dns_recordset.rs1]
}

# Пишем в него информацию о созданных инстансах
resource "null_resource" "ansible_config2" {
  count = var.instance_params.count

  provisioner "local-exec" {
    command = "echo 'instance${count.index} ansible_host=${var.clouddns.instances_prefix}${count.index}.${var.clouddns.domain} ansible_connection=ssh ansible_user=ubuntu ansible_ssh_private_key_file=${var.environment.private_key_file}' >>tmp/hosts"
  }

  depends_on = [null_resource.ansible_config1]
}

# Также дописываем в инвентори файл информацию о сервере с раннером, системой мониторинга и графаной
resource "null_resource" "ansible_config3" {

  provisioner "local-exec" {
    command = "echo ''>>tmp/hosts; echo '[runners]'>>tmp/hosts"
  }

  provisioner "local-exec" {
    command = "echo 'runner0 ansible_host=${var.clouddns.runners_prefix}0.${var.clouddns.domain} ansible_connection=ssh ansible_user=ubuntu ansible_ssh_private_key_file=${var.environment.private_key_file}' >>tmp/hosts"
  }

  provisioner "local-exec" {
    command = "echo ''>>tmp/hosts; echo '[monitoring]'>>tmp/hosts"
  }

  provisioner "local-exec" {
    command = "echo 'monitoring0 ansible_host=${var.clouddns.monitoring_prefix}0.${var.clouddns.domain} ansible_connection=ssh ansible_user=ubuntu ansible_ssh_private_key_file=${var.environment.private_key_file}' >>tmp/hosts"
  }

  provisioner "local-exec" {
    command = "echo ''>>tmp/hosts; echo '[grafana]'>>tmp/hosts"
  }

  provisioner "local-exec" {
    command = "echo 'grafana0 ansible_host=${var.clouddns.grafana_prefix}0.${var.clouddns.domain} ansible_connection=ssh ansible_user=ubuntu ansible_ssh_private_key_file=${var.environment.private_key_file}' >>tmp/hosts"
  }

  depends_on = [null_resource.ansible_config2]
}


# Делаем паузу для запуска инстансов
resource "null_resource" "ansible_waiting" {
  # Выполняем playbook, но перед этим делаем паузу на 240 секунд, чтобы хост успел гарантированно загрузиться
  provisioner "local-exec" {
    command = "/bin/sleep 240"
  }

  depends_on = [null_resource.ansible_config3]
}

# Выполнение ansible-playbook для конфигурации инстансов после их создания
resource "null_resource" "ansible_run" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i ./tmp/hosts -v infra_playbook.yaml"
  }

  depends_on = [null_resource.ansible_waiting]
}

# Конфигурирование вспомогательных инстансов можно проводить сразу после запуска инстансов
# Выполнение ansible-playbook для конфигурации истанса с раннерами после их создания
resource "null_resource" "ansible_run2" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i ./tmp/hosts -vvv runner_playbook.yaml"
  }

  depends_on = [null_resource.ansible_run]
}

# Выполнение ansible-playbook для конфигурации истанса с системой мониторинга
resource "null_resource" "ansible_monitoring" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i ./tmp/hosts -v monitoring_playbook.yaml"
  }

  depends_on = [null_resource.ansible_run2]
}

# Выполнение ansible-playbook для конфигурации истанса c графаной
resource "null_resource" "ansible_grafana" {

  # Выполняем playbook
  provisioner "local-exec" {
    command = "ansible-playbook -b -i ./tmp/hosts -v grafana_playbook.yaml"
  }

  depends_on = [null_resource.ansible_monitoring]
}

# Если нужно развернуть сервис, то разворачиваем его
# Выполнение ansible-playbook после создания всех инстансов
resource "null_resource" "service_deployment" {
  count = var.environment.install_service ? 1 : 0

  # Клонируем репозиторий с плейбуком установки сервиса
  provisioner "local-exec" {
    command = "rm -r ./tmp/service 2>/dev/null; git clone ${var.environment.service_repository} ./tmp/service"
  }

  # Разворачиваем сервис
  provisioner "local-exec" {
    command = "cp -f ./.ssh ./tmp/service/${var.environment.service_playbook_folder}; cp -f ./tmp/hosts ./tmp/service/${var.environment.service_playbook_folder}; cd ./tmp/service/${var.environment.service_playbook_folder}; ansible-playbook -b -i hosts -vvv ${var.environment.service_playbook_file}"
  }

  # Удаляем временные файлы
  provisioner "local-exec" {
    command = "rm -r ./tmp/service"
  }

  depends_on = [null_resource.ansible_run]
}

# Выводим информацию об IP-адресах созданных виртуальных машин
output "external_ip_computers" {
 value = [yandex_compute_instance_group.ig-1.instances[*].network_interface[0].nat_ip_address]
}

# Выводим информацию об IP-адресе балансировщика
output "external_ip_address_load_balancer" {
  value = [ for list in yandex_lb_network_load_balancer.load-balancer-1.listener: list.external_address_spec.*.address ][0]
}
