#!/bin/bash

echo "Вы запустили скрипт разворачивания инфраструктуры проекта."
echo -n "Продолжим (Y/n)? "

read AGREEMENT

if [[ $AGREEMENT != "n" ]] && [[ $AGREEMENT != "N" ]] 
then
  echo "*******************************************"
  echo "*   Создание bucket                       *"
  echo "*******************************************"
  cd bucket
  terraform apply -auto-approve

  echo "*******************************************"
  echo "*   Создание environment                  *"
  echo "*******************************************"  
  cd ../environment
  terraform apply -auto-approve
    
  echo "*******************************************"
  echo "*   Создание test                         *"
  echo "*******************************************"  
  cd ../test
  terraform apply -auto-approve
  
  echo "*******************************************"
  echo "*   Создание prod                         *"
  echo "*******************************************"
  cd ../prod
  terraform apply -auto-approve
     
  echo "Развертка инфраструктуры завершена. "
else
  echo "Развертка инфраструктуры отменена."
fi

